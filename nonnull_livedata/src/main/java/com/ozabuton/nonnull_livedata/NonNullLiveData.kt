package com.ozabuton.nonnull_livedata

import androidx.annotation.MainThread
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

open class NonNullLiveData<T : Any> private constructor() : LiveData<T>() {

    private val observers = mutableMapOf<NonNullObserver<T>, Observer<T>>()

    constructor(initialValue: T) : this() {
        value = initialValue
    }

    @MainThread
    @Deprecated(
        message = "Use observe for NonNullObserver.",
        replaceWith = ReplaceWith("observe(owner, nonNullObserver)"),
        level = DeprecationLevel.HIDDEN
    )
    override fun observe(owner: LifecycleOwner, observer: Observer<in T>) {
        super.observe(owner, observer)
    }

    @MainThread
    @Deprecated(
        message = "Use observeForever for NonNullObserver.",
        replaceWith = ReplaceWith("observeForever(nonNullObserver)"),
        level = DeprecationLevel.HIDDEN
    )
    override fun observeForever(observer: Observer<in T>) {
        super.observeForever(observer)
    }

    @MainThread
    @Deprecated(
        message = "Use removeObserver for NonNullObserver.",
        replaceWith = ReplaceWith("removeObserver(nonNullObserver)"),
        level = DeprecationLevel.HIDDEN
    )
    override fun removeObserver(observer: Observer<in T>) {
        super.removeObserver(observer)
    }

    @MainThread
    open fun observe(owner: LifecycleOwner, nonNullObserver: NonNullObserver<T>) {
        val observer = Observer<T> { nonNullObserver.onChanged(it!!) }
        observers[nonNullObserver] = observer
        super.observe(owner, observer)
    }

    @MainThread
    open fun observe(lifecycleOwner: LifecycleOwner, nonNullObserver: (T) -> Unit) {
        observe(lifecycleOwner, NonNullObserver { nonNullObserver(it) })
    }

    @MainThread
    open fun observeForever(nonNullObserver: NonNullObserver<T>) {
        val observer = Observer<T> { nonNullObserver.onChanged(it) }
        observers[nonNullObserver] = observer
        super.observeForever(observer)
    }

    @MainThread
    open fun observeForever(nonNullObserver: (T) -> Unit) {
        observeForever(NonNullObserver { nonNullObserver(it) })
    }

    @MainThread
    open fun removeObserver(nonNullObserver: NonNullObserver<T>) {
        val observer = observers[nonNullObserver]
        if (observer != null) {
            observers.remove(nonNullObserver)
            super.removeObserver(observer)
        }
    }

    override fun getValue(): T {
        return super.getValue() ?: throw UninitializedPropertyAccessException()
    }

    @MainThread
    override fun setValue(value: T) {
        super.setValue(value)
    }

    @MainThread
    open fun <Y : Any> map(transform: (T) -> Y): NonNullLiveData<Y> {
        return NonNullTransformations.map(this, transform)
    }

    @MainThread
    open fun <Y : Any> switchMap(transform: (T) -> NonNullLiveData<Y>): NonNullLiveData<Y> {
        return NonNullTransformations.switchMap(this, transform)
    }

    @MainThread
    open fun distinctUntilChanged(): NonNullLiveData<T> {
        return NonNullTransformations.distinctUntilChanged(this)
    }
}

