package com.ozabuton.nonnull_livedata;

import androidx.annotation.NonNull;

public interface NonNullObserver<T> {
    void onChanged(@NonNull T t);
}
