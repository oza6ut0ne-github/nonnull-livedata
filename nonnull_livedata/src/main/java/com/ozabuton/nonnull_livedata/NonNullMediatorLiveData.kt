package com.ozabuton.nonnull_livedata

import androidx.annotation.CallSuper
import androidx.annotation.MainThread
import androidx.arch.core.internal.SafeIterableMap
import androidx.lifecycle.LiveData

class NonNullMediatorLiveData<T : Any>(initialValue: T) : NonNullMutableLiveData<T>(initialValue) {
    private val mSources = SafeIterableMap<NonNullLiveData<*>, Source<*>>()
    @MainThread
    fun <S : Any> addSource(source: NonNullLiveData<S>, onChanged: NonNullObserver<in S>) {
        val e = Source(source, onChanged)
        val existing = mSources.putIfAbsent(source, e)
        require(!(existing != null && existing.mObserver !== onChanged)) {
            "This source was already added with the different observer"
        }
        if (existing != null) {
            return
        }
        if (hasActiveObservers()) {
            e.plug()
        }
    }

    @MainThread
    fun <S : Any> addSource(source: NonNullLiveData<S>, onChanged: (S) -> Unit) {
        addSource(source, NonNullObserver { onChanged(it) })
    }


    @MainThread
    fun <S : Any> removeSource(toRemote: NonNullLiveData<S>) {
        mSources.remove(toRemote)?.unplug()
    }

    @CallSuper
    override fun onActive() {
        for ((_, value) in mSources) {
            value.plug()
        }
    }

    @CallSuper
    override fun onInactive() {
        for ((_, value) in mSources) {
            value.unplug()
        }
    }

    private class Source<V : Any> internal constructor(
        internal val mLiveData: NonNullLiveData<V>,
        internal val mObserver: NonNullObserver<in V>
    ) : NonNullObserver<V> {
        internal var mVersion = LiveData::class.java.getDeclaredField("START_VERSION").apply {
            isAccessible = true
        }.getInt(null)

        internal fun plug() {
            mLiveData.observeForever(this)
        }

        internal fun unplug() {
            mLiveData.removeObserver(this)
        }

        override fun onChanged(v: V) {
            LiveData::class.java.getDeclaredMethod("getVersion").apply {
                isAccessible = true
            }.invoke(mLiveData).let {
                if (mVersion != (it as Int)) {
                    mVersion = it
                    mObserver.onChanged(v)
                }
            }
        }
    }
}
