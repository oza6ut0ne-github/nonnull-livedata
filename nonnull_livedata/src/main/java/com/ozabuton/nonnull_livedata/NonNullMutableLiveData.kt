package com.ozabuton.nonnull_livedata

open class NonNullMutableLiveData<T : Any>(initialValue: T) : NonNullLiveData<T>(initialValue) {
    public override fun postValue(value: T) {
        super.postValue(value)
    }

    public override fun setValue(value: T) {
        super.setValue(value)
    }
}
