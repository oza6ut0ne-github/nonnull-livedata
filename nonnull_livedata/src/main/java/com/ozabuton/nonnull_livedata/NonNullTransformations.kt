package com.ozabuton.nonnull_livedata

import androidx.annotation.MainThread

object NonNullTransformations {

    @JvmStatic
    @MainThread
    fun <X : Any, Y : Any> map(
        source: NonNullLiveData<X>, mapFunction: (X) -> Y
    ): NonNullLiveData<Y> {
        val result = NonNullMediatorLiveData(mapFunction(source.value))
        result.addSource(source) { x -> result.value = mapFunction(x) }
        return result
    }

    @JvmStatic
    @MainThread
    fun <X : Any, Y : Any> switchMap(
        source: NonNullLiveData<X>,
        switchMapFunction: (X) -> NonNullLiveData<Y>
    ): NonNullLiveData<Y> {
        val result = NonNullMediatorLiveData(switchMapFunction(source.value).value)
        result.addSource(source, object : NonNullObserver<X> {
            var mSource: NonNullLiveData<Y>? = null

            override fun onChanged(x: X) {
                val newLiveData = switchMapFunction(x)
                if (mSource === newLiveData) {
                    return
                }
                if (mSource != null) {
                    result.removeSource(mSource!!)
                }
                mSource = newLiveData
                if (mSource != null) {
                    result.addSource(mSource!!) { y -> result.value = y }
                }
            }
        })
        return result
    }

    @JvmStatic
    @MainThread
    fun <X : Any> distinctUntilChanged(source: NonNullLiveData<X>): NonNullLiveData<X> {
        val outputLiveData = NonNullMediatorLiveData(source.value)
        outputLiveData.addSource(source) { currentValue ->
            val previousValue = outputLiveData.value
            if (previousValue != currentValue) {
                outputLiveData.value = currentValue
            }
        }
        return outputLiveData
    }
}

